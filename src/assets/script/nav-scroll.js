// Sélectionnez votre barre de navigation
const nav = document.querySelector('nav');

// Ajoutez un écouteur d'événement pour détecter le défilement de la page
window.addEventListener('scroll', () => {
    // Si la position de défilement de la page est supérieure à 0, cela signifie que l'utilisateur a fait défiler la page
    if (window.scrollY > 0) {
        // Ajoutez une classe "scrolled" à la barre de navigation
        nav.classList.add('scrolled');
    } else {
        // Sinon, supprimez la classe "scrolled"
        nav.classList.remove('scrolled');
    }
});